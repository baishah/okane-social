import { TrackingPage } from './../../pages/tracking/tracking';
import { TripService } from './../../services/trip-service';
import { Platform, DomController, NavController } from 'ionic-angular';
import { Component, Input, ElementRef, Renderer } from '@angular/core';



/**
 * Generated class for the ContentDrawerComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'content-drawer',
  templateUrl: 'content-drawer.html'
})
export class ContentDrawerComponent  {
 
  cards = [
    {
      imageUrl: 'assets/img/card/card-saopaolo.png',
      title: 'São Paulo',
      subtitle: '41 Listings'
    },
    {
      imageUrl: 'assets/img/card/card-amsterdam.png',
      title: 'Amsterdam',
      subtitle: '64 Listings'
    },
    {
      imageUrl: 'assets/img/card/card-sf.png',
      title: 'San Francisco',
      subtitle: '72 Listings'
    },
    {
      imageUrl: 'assets/img/card/card-madison.png',
      title: 'Madison',
      subtitle: '28 Listings'
    }];

  @Input('options') options: any;
  
  animateClass: any;
  handleHeight: number = 1;
  bounceBack: boolean = true;
  thresholdTop: number = 200;
  thresholdBottom: number = 200;
  origin:any;
 
  constructor(public navCtrl: NavController,public element: ElementRef, public renderer: Renderer, public domCtrl: DomController, public platform: Platform) {
 
  }
  
  cardTapped(card) {
    alert(card.title + ' was tapped.');
  }

  ngAfterViewInit() {
 
    if(this.options.handleHeight){
      this.handleHeight = this.options.handleHeight;
    }
 
    if(this.options.bounceBack){
      this.bounceBack = this.options.bounceBack;
    }
 
    if(this.options.thresholdFromBottom){
      this.thresholdBottom = this.options.thresholdFromBottom;
    }
 
    if(this.options.thresholdFromTop){
      this.thresholdTop = this.options.thresholdFromTop;
    }
 
    this.renderer.setElementStyle(this.element.nativeElement, 'top', this.platform.height() - this.handleHeight + 'px');
    this.renderer.setElementStyle(this.element.nativeElement, 'padding-top', this.handleHeight + 'px');
    

    //let hammer = new window['Hammer'](this.element.nativeElement);
    let hammer = new window['Hammer'](this.element.nativeElement.children[0]); //make the header as handleHeight
    hammer.get('pan').set({ direction: window['Hammer'].DIRECTION_VERTICAL });
 
    hammer.on('pan', (ev) => {
      this.handlePan(ev);
    });
 
  }
 
  handlePan(ev){
 
    let newTop = ev.center.y;
 
    let bounceToBottom = false;
    let bounceToTop = false;
 
    if(this.bounceBack && ev.isFinal){
 
      let topDiff = newTop - this.thresholdTop;
      let bottomDiff = (this.platform.height() - this.thresholdBottom) - newTop;     
 
      topDiff >= bottomDiff ? bounceToBottom = true : bounceToTop = true;
 
    }
 
    if((newTop < this.thresholdTop && ev.additionalEvent === "panup") || bounceToTop){
      
      this.domCtrl.write(() => {
        this.renderer.setElementStyle(this.element.nativeElement, 'transition', 'top 0.5s');
        this.renderer.setElementStyle(this.element.nativeElement, 'top', '0px');
        //this.renderer.setElementStyle(this.element.nativeElement, 'width', '100%');
        //this.renderer.setElementStyle(this.element.nativeElement, 'margin','0px 0px 0px 0px');
        
      });
 
    } else if(((this.platform.height() - newTop) < this.thresholdBottom && ev.additionalEvent === "pandown") || bounceToBottom){
 
      this.domCtrl.write(() => {
        this.renderer.setElementStyle(this.element.nativeElement, 'transition', 'top 0.5s');
        this.renderer.setElementStyle(this.element.nativeElement, 'top', this.platform.height() - this.handleHeight + 'px');
        //this.renderer.setElementStyle(this.element.nativeElement, 'width','85%');
        //this.renderer.setElementStyle(this.element.nativeElement, 'margin-left','20px');
         });
 
    } else {
 
      this.renderer.setElementStyle(this.element.nativeElement, 'transition', 'none');
 
      if(newTop > 0 && newTop < (this.platform.height() - this.handleHeight)) {
 
        if(ev.additionalEvent === "panup" || ev.additionalEvent === "pandown"){
 
          this.domCtrl.write(() => {
            this.renderer.setElementStyle(this.element.nativeElement, 'top', newTop + 'px');
          });
 
        }
 
      }
 
    }
 
  }

  showMore(){
    this.navCtrl.push(ContentDrawerComponent);
  }
 
}

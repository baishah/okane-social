import { Component } from '@angular/core';

/**
 * Generated class for the PortsComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'ports',
  templateUrl: 'ports.html'
})
export class PortsComponent {

  text: string;

  constructor() {
    console.log('Hello PortsComponent Component');
    this.text = 'Hello World';
  }

}

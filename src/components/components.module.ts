import { NgModule } from '@angular/core';
import { ContentDrawerComponent } from './content-drawer/content-drawer';
import { PortsComponent } from './ports/ports';
@NgModule({
	declarations: [ContentDrawerComponent,
    PortsComponent],
	imports: [],
	exports: [ContentDrawerComponent,
    PortsComponent]
})
export class ComponentsModule {}

import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Camera } from '@ionic-native/camera';
/*
  Generated class for the CameraProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class CameraProvider {

  constructor(private camera:Camera) {
  }

  getPictureFromCamera(){
    return this.getImage(this.camera.PictureSourceType.CAMERA, true);
  }

  getPictureFromPhotoLibrary(){
    return this.getImage(this.camera.PictureSourceType.PHOTOLIBRARY);
  }

  //editPicture
  getImage(pictureSourceType,crop=false,quality=50,allowEdit=true,saveToAlbum=true){
    const options = {
      quality,
      allowEdit,
      destinationType:this.camera.DestinationType.DATA_URL,
      sourceType:pictureSourceType,
      encodingType:this.camera.EncodingType.PNG,
      saveToPhotoAlbum:saveToAlbum
    };

  //if crop set to size 600*600
  if(crop){
    options['targetWidth']=600;
    options['targetHeight']=600;
  }

  return this.camera.getPicture(options).then(imageData=>{
    const base64Image ='data:image/png;base64,' + imageData;
    return base64Image;
  },error =>{
    console.log('CAMERA ERROR ->' + JSON.stringify(error));
  });
  }
}

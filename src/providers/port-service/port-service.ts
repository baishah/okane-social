import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { delay } from 'rxjs/operators';
import { Country, Port } from '../../types';

/*
  Generated class for the PortServiceProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class PortServiceProvider {

  private countries: Country[] = [new Country({
    id: 0,
    name: 'United States',
    flag: 'us',
    code:'USD',
    ports: [
        { id: 0, name: 'US Dollar' }
    ]
}), new Country({
    id: 2,
    name: '19 states of the EU',
    flag: 'eu',
    code:'EUR',
    ports: [
        { id: 2, name: 'European Euro' }
    ]
}), new Country({
    id: 3,
    name: 'Japan',
    flag: 'jp',
    code:'JPY',
    ports: [
        { id: 3, name: 'Japanese Yen' }
    ]
}), new Country({
    id: 4,
    name: 'United Kingdom',
    flag: 'uk',
    code:'GBP',
    ports: [
        { id: 4, name: 'Pound Sterling' }
    ]
}), new Country({
    id: 6,
    name: 'Australia',
    flag: 'au',
    code:'AUD',
    ports: [
        { id: 6, name: 'Australian Dollar' }
    ]
}), new Country({
    id: 7,
    name: 'Canada',
    flag: 'ca',
    code:'CAD',
    ports: [
        { id: 7, name: 'Canadian Dollar' }
    ]
}), new Country({
    id: 8,
    name: 'Switzerland',
    flag: 'sz',
    code:'CHF',
    ports: [
        { id: 8, name: 'Swiss Franc' }
    ]
}), new Country({
    id: 9,
    name: 'China',
    flag: 'ch',
    code:'CNY',
    ports: [
        { id: 9, name: 'Chinese Yuan Renminbi' }
    ]
}), new Country({
    id: 11,
    name: 'Sweden',
    flag: 'sw',
    code:'SEK',
    ports: [
        { id: 11, name: 'Swedish Krona' },
    ]
}), new Country({
    id: 12,
    name: 'Mexico',
    flag: 'mx',
    code:'MXN',
    ports: [
        { id: 14, name: 'Mexican Peso' },
    ]
}), new Country({
    id: 13,
    name: 'New Zealand',
    flag: 'nz',
    code:'NZD',
    ports: [
        { id: 17, name: 'New Zealand Dollar' }
    ]
}), new Country({
    id: 14,
    name: 'Singapore',
    flag: 'sg',
    code:'SGD',
    ports: [
        { id: 18, name: 'Singapore Dollar' }
    ]
}), new Country({
    id: 15,
    name: 'Hong Kong(China)',
    flag: 'hk',
    code:'HKD',
    ports: [
        { id: 19, name: 'Hong Kong Dollar' }
    ]
}), new Country({
    id: 5,
    name: 'Norway',
    flag: 'nw',
    code:'NOK',
    ports: [
        { id: 51, name: 'Norwegian Krone' }
    ]
}), new Country({
    id: 16,
    name: 'South Korea',
    flag: 'sk',
    code:'KRW',
    ports: [
        { id: 21, name: 'South Korean Won' }
    ]
}), new Country({
    id: 17,
    name: 'Turkey',
    flag: 'tu',
    code:'TRY',
    ports: [
        { id: 22, name: 'Turkish Lira' }
    ]
}), new Country({
    id: 18,
    name: 'India',
    flag: 'in',
    code:'INR',
    ports: [
        { id: 23, name: 'Indian Rupee' }
    ]
}), new Country({
    id: 19,
    name: 'Russia',
    flag: 'ru',
    code:'RUB',
    ports: [
        { id: 38, name: 'Russian Ruble' },
    ]
}), new Country({
    id: 20,
    name: 'Brazil',
    flag: 'br',
    code:'BRL',
    ports: [
        { id: 25, name: 'Brazilian Real' }
    ]
}), new Country({
    id: 21,
    name: 'South Africa',
    flag: 'sa',
    code:'ZAR',
    ports: [
        { id: 26, name: 'South African Rand' }
    ]
}), new Country({
    id: 22,
    name: 'Denmark',
    flag: 'dn',
    code:'DKK',
    ports: [
        { id: 39, name: 'Danish Krone' }
    ]
}), new Country({
    id: 23,
    name: 'Poland',
    flag: 'po',
    code:'PLN',
    ports: [
        { id: 28, name: 'Polish Zloty' }
    ]
}), new Country({
    id: 24,
    name: 'Taiwan',
    flag: 'ta',
    code:'TWD',
    ports: [
        { id: 29, name: 'New Taiwan Dollar' }
    ]
}), new Country({
    id: 25,
    name: 'Thailand',
    flag: 'th',
    code:'THB',
    ports: [
        { id: 30, name: 'Thai Baht' }
    ]
}), new Country({
    id: 26,
    name: 'Malaysia',
    flag: 'my',
    code:'MYR',
    ports: [
        { id: 31, name: 'Malaysian Ringgit' }
    ]
})];

getCountries(page?: number, size?: number): Country[] {
    let countries = [];

    if (page && size) {
        countries = this.countries.slice((page - 1) * size, ((page - 1) * size) + size);
    } else {
        countries = this.countries;
    }

    return countries;
}

getPorts(page?: number, size?: number): Port[] {
    let ports = [];

    this.countries.forEach(country => {
        country.ports.forEach(port => {
            port.country = country;
            ports.push(port);
        });
    });

    if (page && size) {
        ports = ports.slice((page - 1) * size, ((page - 1) * size) + size);
    }

    return ports;
}

getPortsAsync(page?: number, size?: number, timeout = 2000): Observable<Port[]> {
    return new Observable<Port[]>(observer => {
        observer.next(this.getPorts(page, size));
        observer.complete();
    }).pipe(delay(timeout));
}

}


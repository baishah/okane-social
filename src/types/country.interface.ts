import { IPort } from './port.interface';

export interface ICountry {
    id: number;
    name: string;
    flag?: string;
    code:string;
    ports?: IPort[];
}

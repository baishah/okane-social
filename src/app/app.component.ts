import { UserPage } from './../pages/user/user';
import { WelcomePage } from './../pages/welcome/welcome';
import { Component } from '@angular/core';
import { Platform } from 'ionic-angular';
import { ViewChild } from '@angular/core';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { WalkthroughPage } from './../pages/walkthrough/walkthrough';
import { DocumentValidPage } from './../pages/document-valid/document-valid';



// import pages
import { LoginPage } from '../pages/login/login';
import { HomePage } from '../pages/home/home';

import { AngularFireAuth } from "angularfire2/auth/auth";
import { AuthService } from "../services/auth-service";
import { Subject } from 'rxjs/Subject';
import { HistoryPage } from '../pages/history/history';

// end import pages

@Component({
  templateUrl: 'app.html',
  queries: {
    nav: new ViewChild('content')
  }
})

export class MyApp {
  rootPage: any;
  nav: any;
  user = {};
  activePage = new Subject();

  pages:Array<{icon:string, title: string, component: any}>;
  rightMenuItems: Array<{ icon: string, active: boolean }>;

  constructor(platform: Platform, statusBar: StatusBar, splashScreen: SplashScreen, public afAuth: AngularFireAuth,
              public authService: AuthService) {

              this.pages=[
                {icon:'ios-list-box', title:'History', component:HomePage},
                {icon:'ios-mail', title:'Notifications', component:HomePage},
                {icon:'ios-help-circle', title:'Help', component:HomePage},
                {icon:'ios-settings', title:'Settings', component:HomePage},
                {icon:'ios-information-circle', title:'About', component:HomePage},
                {icon:'ios-star', title:'Rate Us!', component:HomePage},
                ]

                this.rightMenuItems = [
                  { icon: 'ios-list-box', active: false },
                  { icon: 'ios-mail', active: false },
                  { icon: 'ios-help-circle', active: false },
                  { icon: 'ios-settings', active: false },
                  { icon: 'ios-information-circle', active: false },
                  { icon: 'ios-star', active: false },
                  ];

    platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      statusBar.styleDefault();
      splashScreen.hide();

      // check for login stage, then redirect
      afAuth.authState.take(1).subscribe(authData => {
        if (authData) {
          this.nav.setRoot(HomePage);
        } else {
          this.nav.setRoot(WalkthroughPage);
        }
      });

      // get user data
      afAuth.authState.subscribe(authData => {
        if (authData) {
          this.user = authService.getUserData();
        }
      });
    });
  }

  // view current user profile
  viewProfile(user) {
    this.nav.push(UserPage, {
      user: this.user
    });
  }

  gotoAccount(){
    this.nav.push(UserPage,{ user: this.authService.getUserData()});//to pass data the id current user
    }

  openPage(page){
    this.nav.setRoot(page.component);
    this.activePage.next(page);
  }

  
  rightMenuClick(item){
    this.rightMenuItems.map(menuItem => menuItem.active = false);
    item.active = true;
  }

  logout() {
    this.authService.logout().then(() => {
      this.nav.setRoot(WalkthroughPage);
    });
  }
}

import { NgModule, ErrorHandler } from '@angular/core';
import { Config,IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { MyApp } from './app.component';
import { BrowserModule } from '@angular/platform-browser';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { HttpModule } from '@angular/http';
import { Geolocation } from '@ionic-native/geolocation';
import { IonicStorageModule } from '@ionic/storage';

// Import the AF2 Module
import { AngularFireModule } from 'angularfire2';
import { AngularFireDatabaseModule } from 'angularfire2/database';
import { AngularFireAuthModule } from 'angularfire2/auth';

// Import moment module
import { MomentModule } from 'angular2-moment';

// import services
import { DriverService } from '../services/driver-service';

import { PlaceService } from '../services/place-service';
import { TripService } from '../services/trip-service';
import { SettingService } from "../services/setting-service";
import { DealService } from "../services/deal-service";
import { AuthService } from "../services/auth-service";

import { HomePage } from '../pages/home/home';
import { LoginPage } from '../pages/login/login';
import { PaymentMethodPage } from '../pages/payment-method/payment-method';
import { PlacesPage } from '../pages/places/places';
import { RegisterPage } from '../pages/register/register';
import { TrackingPage } from '../pages/tracking/tracking';
import { MapPage } from "../pages/map/map";
import { UserPage } from '../pages/user/user';
import { WelcomePage } from './../pages/welcome/welcome';
import{ ContentDrawerComponent } from '../components/content-drawer/content-drawer';
import { WalkthroughPage } from './../pages/walkthrough/walkthrough';
import { RestProvider } from '../providers/rest/rest';
import { DocumentValidPage } from './../pages/document-valid/document-valid';
import { HttpClientModule, HttpClient } from '@angular/common/http';
import { CameraProvider } from '../providers/camera/camera';
import {Camera} from '@ionic-native/camera';
import { SocialDetailsPage } from './../pages/social-details/social-details';
import { NotificationsPage } from './../pages/notifications/notifications';
import { MenupopoverPage} from './../pages/menupopover/menupopover';
import { MODE_MD } from 'ionic-angular/config/mode-registry';

//import ionic-moda-transition-pack
import {
  ModalEnterDirect, ModalLeaveDirect
  ,ModalEnterFadeIn, ModalLeaveFadeOut
  ,ModalEnterZoomIn, ModalLeaveZoomIn
  ,ModalEnterZoomOut, ModalLeaveZoomOut
} from '../classes/ionic-modal-transition-pack';
import { PortServiceProvider } from '../providers/port-service/port-service';
import { FindPortsPage } from '../pages/find-ports/find-ports';
import { ModalPage } from '../pages/modal/modal';
import { SelectSearchableModule } from 'ionic-select-searchable';
import { PortsItemTemplateDirective } from '../components/ports/ports-item-template';
import { PortsTitleTemplateDirective } from '../components/ports/ports-title-template';
import { PortsValueTemplateDirective } from '../components/ports/ports-value-template';
import { PortsComponent } from '../components/ports/ports';
import {HistoryPage} from './../pages/history/history';
import { SettingsPage } from '../pages/settings/settings';

export const firebaseConfig = {
  apiKey: "AIzaSyB-BvOIOUeXDYKzpAdhBSxW4Y8OtF-x8As",
  authDomain: "okanedb.firebaseapp.com",
  databaseURL: "https://okanedb.firebaseio.com",
  projectId: "okanedb",
  storageBucket: "okanedb.appspot.com",
  messagingSenderId: "285612603743"
};

@NgModule({
  declarations: [
    MyApp,
    HomePage,
    LoginPage,
    PaymentMethodPage,
    PlacesPage,
    RegisterPage,
    TrackingPage,
    MapPage,
    UserPage,
    WelcomePage,
    ContentDrawerComponent,
    WalkthroughPage,
    DocumentValidPage,
    SocialDetailsPage,
    NotificationsPage,
    MenupopoverPage,
    ModalPage,
    FindPortsPage,
    PortsItemTemplateDirective,
    PortsTitleTemplateDirective,
    PortsValueTemplateDirective,
    PortsComponent,
    HistoryPage,
    SettingsPage,
  ],
  imports: [
    BrowserModule,
    HttpModule,
    HttpClientModule,
    IonicStorageModule.forRoot(),
    AngularFireModule.initializeApp(firebaseConfig),
    AngularFireDatabaseModule,
    AngularFireAuthModule,
    MomentModule,
    SelectSearchableModule,
    // IonicModule.forRoot(MyApp,{
    //   mode: 'ios'
    // })

    IonicModule.forRoot(MyApp,{
      mode: 'ios',
      scrollPadding: false,
      scrollAssist: false
    })
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    LoginPage,
    PaymentMethodPage,
    PlacesPage,
    RegisterPage,
    TrackingPage,
    MapPage,
    UserPage,
    WelcomePage,
    ContentDrawerComponent,
    WalkthroughPage,
    DocumentValidPage,
    SocialDetailsPage,
    NotificationsPage,
    MenupopoverPage,
    ModalPage,
    FindPortsPage,
    PortsComponent,
    HistoryPage,
    SettingsPage
    
  ],
  providers: [
    StatusBar,
    SplashScreen,
    Geolocation,
    DriverService,
    PlaceService,
    TripService,
    SettingService,
    DealService,
    AuthService,
    { provide: ErrorHandler, useClass: IonicErrorHandler},
    RestProvider,
    CameraProvider,
    Camera,
    PortServiceProvider,
  ]
})
export class AppModule {
  constructor(public config: Config) {
    this.setCustomTransitions();
}

private setCustomTransitions() {
    this.config.setTransition('ModalEnterDirect', ModalEnterDirect);
    this.config.setTransition('ModalLeaveDirect', ModalLeaveDirect);
    
    this.config.setTransition('ModalEnterFadeIn', ModalEnterFadeIn);
    this.config.setTransition('ModalLeaveFadeOut', ModalLeaveFadeOut);
    
    this.config.setTransition('ModalEnterZoomIn', ModalEnterZoomIn);
    this.config.setTransition('ModalLeaveZoomIn', ModalLeaveZoomIn);
    
    this.config.setTransition('ModalEnterZoomOut', ModalEnterZoomOut);
    this.config.setTransition('ModalLeaveZoomOut', ModalLeaveZoomOut);
}
}

import { HomePage } from './../home/home';
import { CameraProvider } from './../../providers/camera/camera';
import { AuthService } from "../../services/auth-service";
import { AngularFireAuth } from 'angularfire2/auth';
import { FirebaseApp } from 'angularfire2';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams,Platform,ActionSheetController,LoadingController} from 'ionic-angular';
import { ENABLE_SIGNUP } from "../../services/constants";
import * as firebase from 'firebase';
import { UserPage } from './../user/user';


/**
 * Generated class for the DocumentValidPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-document-valid',
  templateUrl: 'document-valid.html',
})
export class DocumentValidPage {

  user:any={};
  isRegisterEnabled = ENABLE_SIGNUP;
  chosenPicture:any={};


  
  constructor(public afAuth : AngularFireAuth,
    public authService: AuthService,
    public navCtrl: NavController, 
    public navParams: NavParams, 
    public actionSheetCtrl:ActionSheetController, 
    public cameraProvider:CameraProvider,
    public platform: Platform,
    public loadingCtrl: LoadingController,
    private firebase:FirebaseApp) {
  }

  changePicture() {

    const actionsheet = this.actionSheetCtrl.create({
      title: 'upload picture',
      buttons: [
        {
          text: 'camera',
          icon: !this.platform.is('ios') ? 'camera' : null,
          handler: () => {
            this.takePicture();
          }
        },
        {
          text: !this.platform.is('ios') ? 'gallery' : 'camera roll',
          icon: !this.platform.is('ios') ? 'image' : null,
          handler: () => {
            this.getPicture();
          }
        },
        {
          text: 'cancel',
          icon: !this.platform.is('ios') ? 'close' : null,
          role: 'destructive',
          handler: () => {
            console.log('the user has cancelled the interaction.');
          }
        }
      ]
    });
    return actionsheet.present();
  }

  takePicture() {
    const loading = this.loadingCtrl.create();

    loading.present();
    return this.cameraProvider.getPictureFromCamera().then(picture => {
      if (picture) {
        this.chosenPicture = picture;     
      }
      loading.dismiss();
    }, error => {
      alert(error);
    });
  }

  getPicture() {
    const loading = this.loadingCtrl.create();

    loading.present();
    return this.cameraProvider.getPictureFromPhotoLibrary().then(picture => {
      if (picture) {
        this.chosenPicture = picture;
      }
      loading.dismiss();
    }, error => {
      alert(error);
    });
  }

  uploadID() {
    this.changePicture();
    let storageRef = firebase.storage().ref();
    const filename = Math.floor(Date.now() / 1000);
    let path = '/users/' + Date.now();
    let iRef = storageRef.child(path);
    iRef.putString(this.chosenPicture, firebase.storage.StringFormat.DATA_URL).then((snapshot)=> {
     // Do something here when the data is succesfully uploaded!
     let url = snapshot.metadata.downloadURLs[0];
     this.firebase.database().ref().child('passengers/' + this.afAuth.auth.currentUser.uid).child('IDcard').set({
      url
    })
  }).catch((error) => {
console.log(error)
});
}

  uploadCard() {
    this.changePicture();
    let storageRef = firebase.storage().ref();
    const filename = Math.floor(Date.now() / 1000);
    let path = '/users/' + Date.now();
    let iRef = storageRef.child(path);
    iRef.putString(this.chosenPicture, firebase.storage.StringFormat.DATA_URL).then((snapshot)=> {
     // Do something here when the data is succesfully uploaded!
     let url = snapshot.metadata.downloadURLs[0];
     this.firebase.database().ref().child('passengers/' + this.afAuth.auth.currentUser.uid).child('Creditcard').set({
      url
    })
  }).catch((error) => {
console.log(error)
});
}

  save() {
    this.authService.updateUserProfile(this.user);
    this.navCtrl.pop();
  }

  savee(){
    this.authService.getUser(this.user.uid).update(this.user).then(data=>{
       this.navCtrl.push(HomePage);
    });
  }

  viewProfile(user) {
    this.navCtrl.push(UserPage, {
      user: this.user
    });
  }
}

import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { DocumentValidPage } from './document-valid';

@NgModule({
  declarations: [
    DocumentValidPage,
  ],
  imports: [
    IonicPageModule.forChild(DocumentValidPage),
  ],
})
export class DocumentValidPageModule {}

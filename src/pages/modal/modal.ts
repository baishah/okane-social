import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams,ViewController } from 'ionic-angular';
import { PortServiceProvider } from '../../providers/port-service/port-service';
import { Port } from '../../types';


/**
 * Generated class for the ModalPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-modal',
  templateUrl: 'modal.html',
})
export class ModalPage {

  port:Port;
  ports:Port[];
  
  constructor(public navCtrl: NavController, public navParams: NavParams,private portService:PortServiceProvider,private viewController: ViewController) {
    this.ports = this.portService.getPorts();
  }

  dismiss(){
    this.viewController.dismiss();
  }
  ionViewDidLoad() {
    console.log('ionViewDidLoad ModalPage');
  }

}

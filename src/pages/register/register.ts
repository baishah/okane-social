import { WalkthroughPage } from './../walkthrough/walkthrough';
import { Component } from '@angular/core';
import { NavController, LoadingController, AlertController } from 'ionic-angular';
import { LoginPage } from '../login/login';
import { HomePage } from "../home/home";
import { UserPage } from "../user/user";
import { AuthService } from "../../services/auth-service";
import { AngularFireAuth } from "angularfire2/auth/auth";
import { DocumentValidPage } from './../document-valid/document-valid';


@Component({
  selector: 'page-register',
  templateUrl: 'register.html'
})
export class RegisterPage {
  email: string = "";
  password: string = "";
  retypePwd:string = "";
  fullname: string = "";
  mobileNumber: string = "";
  user:any= { photoURL: 'https://marketplace.canva.com/MAB2sqFKHu0/1/thumbnail/canva-business-people-design-person-icon--MAB2sqFKHu0.png' };

  constructor(public nav: NavController, public authService: AuthService, public alertCtrl: AlertController,public loadingCtrl: LoadingController, public afauth:AngularFireAuth) {
    afauth.authState.subscribe(authData => {
      if (authData) {
        this.user = authService.getUserData();
      }
    });
  }

  signup() {
    if(this.email.length == 0 || this.password.length !== this.retypePwd.length || this.fullname.length == 0 || this.mobileNumber.length == 0){
      this.alertCtrl.create({ subTitle:'Invalid Credentials or Password Do not Match', buttons: ['ok']}).present();
    }
    else{
      let loading = this.loadingCtrl.create({ content: 'Creating Account...'});
      loading.present();
      this.authService.register(this.email, this.password, this.fullname, this.mobileNumber).subscribe(authData => {
        loading.dismiss();
        this.nav.setRoot(DocumentValidPage)
      }, error => {
        loading.dismiss();
        let alert = this.alertCtrl.create({
          message: error.message,
          buttons: ['OK']
        });
        alert.present();
      });
    }
    
  }

  login() {
    this.nav.setRoot(LoginPage);
  }

  back(){
    this.nav.push(WalkthroughPage);
  }

  docUpload() {
    this.nav.push(DocumentValidPage);
  }
}

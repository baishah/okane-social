import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { FindPortsPage } from './find-ports';

@NgModule({
  declarations: [
    FindPortsPage,
  ],
  imports: [
    IonicPageModule.forChild(FindPortsPage),
  ],
})
export class FindPortsPageModule {}

import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams,AlertController, LoadingController, ToastController } from 'ionic-angular';
import { RegisterPage } from './../register/register';
import { HomePage } from './../home/home';
import { AuthService } from "../../services/auth-service";
import * as firebase from 'firebase';
import { ENABLE_SIGNUP } from '../../services/constants';

/**
 * Generated class for the WalkthroughPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-walkthrough',
  templateUrl: 'walkthrough.html',
})
export class WalkthroughPage {

  email:string="";
  password:string="";
  isRegisterEnabled:any=true;

  constructor(public navCtrl: NavController, public navParams: NavParams, public authService:AuthService, public alertCtrl:AlertController, public LoadingCtrl:LoadingController, public toast:ToastController) {
    this.isRegisterEnabled = ENABLE_SIGNUP;
  }

  signup(){
    this.navCtrl.push(RegisterPage);
  }

  reset(){
    if(this.email){
      firebase.auth().sendPasswordResetEmail(this.email)
      .then(data=>
        this.toast.create({message:'Please check your email',duration:3000}).present())
        .catch( err => this.toast.create({ message:err.message,duration:3000}).present())
    }
  }

  login(){
    if(this.email.length == 0 || this.password.length == 0){
      this.alertCtrl.create({subTitle:'Invalid Credentials',buttons:['ok']}).present();
    }

    else{
      let loading = this.LoadingCtrl.create({content:'Authenticating...'});
      loading.present();

      this.authService.login(this.email,this.password).then(authData=>{
        loading.dismiss();
        this.navCtrl.setRoot(HomePage);
      },error=>{
        loading.dismiss();
        let alert = this.alertCtrl.create({
          message:error.message,
          buttons:['OK']
        });
        alert.present();
      });
    }
  }

  logout() {
    this.authService.logout().then(() => {
      this.navCtrl.setRoot(WalkthroughPage);
    });
  }
}

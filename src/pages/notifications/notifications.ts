import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams,ModalController,ViewController } from 'ionic-angular';
import { MenupopoverPage } from '../menupopover/menupopover';

/**
 * Generated class for the NotificationsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-notifications',
  templateUrl: 'notifications.html',
})
export class NotificationsPage {

  constructor(public viewCtrl:ViewController,public modalCtrl:ModalController,public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad NotificationsPage');
  }

  close() {
    this.viewCtrl.dismiss();
  }

  menuModal(){
    let contactModal = this.modalCtrl.create(MenupopoverPage, {someVar: 'someValue'}, {
      showBackdrop: false,   // <-- this is not support yet
      enableBackdropDismiss: false,
      enterAnimation: 'ModalEnterFadeIn',
      leaveAnimation: 'ModalLeaveFadeOut'                
  });
    contactModal.present();
  }
}

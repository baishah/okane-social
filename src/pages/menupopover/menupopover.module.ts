import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MenupopoverPage } from './menupopover';

@NgModule({
  declarations: [
    MenupopoverPage,
  ],
  imports: [
    IonicPageModule.forChild(MenupopoverPage),
  ],
})
export class MenupopoverPageModule {}

import { Component } from '@angular/core';
import { IonicPage } from 'ionic-angular';
import { AuthService } from "../../services/auth-service";
import { ViewController, PopoverController, LoadingController, NavController, ModalController, AlertController, NavParams, ToastController } from 'ionic-angular';
import {NotificationsPage} from '../notifications/notifications';
import { WalkthroughPage } from '../walkthrough/walkthrough';
import { UserPage } from '../user/user';
import { HistoryPage } from '../history/history';
import { SettingsPage } from '../settings/settings';

/**
 * Generated class for the MenupopoverPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-menupopover',
  templateUrl: 'menupopover.html',
})
export class MenupopoverPage {

  user:{};
  constructor(public authService: AuthService, public viewCtrl: ViewController, public popoverCtrl: PopoverController, public loadingCtrl: LoadingController, public toast: ToastController, public navParams: NavParams, public nav: NavController,  public modalCtrl: ModalController, public alertCtrl: AlertController){ }

  ionViewDidLoad() {
    console.log('ionViewDidLoad MenupopoverPage');
  }

  gotoAccount(user) {this.nav.push(UserPage, {user: this.authService.getUserData()});
  } 

  gotoHistory(){
    this.nav.push(HistoryPage);
    this.viewCtrl.dismiss();//to dismiss the popover menu
  }

  gotoNotification(){
    this.nav.push(NotificationsPage);
    this.viewCtrl.dismiss();
  }

  gotosetting(){
    this.nav.push(SettingsPage);
    this.viewCtrl.dismiss();
  }

  logout() {
    this.authService.logout().then(() => {
      this.nav.setRoot(WalkthroughPage);
    }); 
  }

  close() {
    this.viewCtrl.dismiss();
  }
}
